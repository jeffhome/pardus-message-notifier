// ==UserScript==
// @name           Pardus Message Notifier
// @namespace      http://userscripts.xcom-alliance.info/
// @description    Plays a sound when having unread message notifications - based closely on the combat notifier from Takius Stargazer
// @author         Miche (Orion) / Sparkle (Artemis)
// @include        http*://*.pardus.at/msgframe.php
// @version        1.3
// @updateURL      http://userscripts.xcom-alliance.info/message_notifier/pardus_message_notifier.meta.js
// @downloadURL    http://userscripts.xcom-alliance.info/message_notifier/pardus_message_notifier.user.js
// @icon           http://userscripts.xcom-alliance.info/message_notifier/icon.png
// @grant          none
// ==/UserScript==
